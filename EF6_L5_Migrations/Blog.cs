﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF6_L5_Migrations
{
    public class Blog
    {
        public int BlogId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        //public int Rating { get; set; }
        public virtual List<Post> Posts { get; set; }

        public override string ToString()
        {
            return $"{BlogId}) {Name}";
        }
    }
}
