﻿using System.Data.Entity;

namespace EF6_L5_Migrations
{
    public class ContextInitializer<T> : CreateDatabaseIfNotExists<BlogContext>
    {
        protected override void Seed(BlogContext db)
        {
            db.Blogs.Add(new Blog { Name = "VovA KachaN" });
            db.Blogs.Add(new Blog { Name = "Resume" });
            db.Blogs.Add(new Blog { Name = ".Net developer" });
            db.SaveChanges();
        }
    }
}