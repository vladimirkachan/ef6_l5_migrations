﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF6_L5_Migrations
{
    class Program
    {
        static void Main(string[] args)
        {
            using BlogContext db = new();
            db.Blogs.Add(new Blog {Name = "Objective: C# Junior"});
            db.SaveChanges();
            var blogs = db.Blogs.ToList();
            Console.WriteLine($@"	Count of blogs = {blogs.Count}");
            foreach(var b in blogs) Console.WriteLine(b);
            //Console.ReadKey();
        }
    }
}
