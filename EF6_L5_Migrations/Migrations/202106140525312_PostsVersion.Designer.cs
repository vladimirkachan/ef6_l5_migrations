﻿// <auto-generated />
namespace EF6_L5_Migrations.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class PostsVersion : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(PostsVersion));
        
        string IMigrationMetadata.Id
        {
            get { return "202106140525312_PostsVersion"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return Resources.GetString("Source"); }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
