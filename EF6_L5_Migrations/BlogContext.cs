﻿using System;
using System.Data.Entity;
using System.Linq;

namespace EF6_L5_Migrations
{
    public class BlogContext : DbContext
    {
        static BlogContext()
        {
            Database.SetInitializer(new ContextInitializer<BlogContext>());
        }
        public BlogContext()
            : base("name=BlogContext")
        {
        }
         public virtual DbSet<Blog> Blogs { get; set; }
         public virtual DbSet<Post> Posts { get; set; }
    }
}